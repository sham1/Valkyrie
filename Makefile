export CC=x86_64-elf-gcc

.PHONY: kernel all clean

kernel: export CFLAGS=\
	-nostartfiles\
	-nostdlib\
	-ffreestanding\
	-O2\
	-Wall\
	-Wextra

kernel:
	${MAKE} -C Bootloader

clean:
	${MAKE} -C Bootloader clean
